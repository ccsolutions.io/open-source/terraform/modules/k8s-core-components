run "custom_installation" {
  command = apply

  variables {
    install_nginx_ingress = false
    install_cert_manager  = true
    install_external_dns  = true
    install_gitlab_agent  = false
    letsencrypt_email     = "letsencrypt@example.com"
    environment           = "test"
    clusterissuer_type    = "http"
  }

  assert {
    condition     = length(helm_release.gitlab_agent) == 0
    error_message = "NGINX Ingress Controller should not be installed"
  }

  assert {
    condition     = helm_release.cert_manager[0].status == "deployed"
    error_message = "Cert Manager should be deployed"
  }

  assert {
    condition     = helm_release.external_dns[0].status == "deployed"
    error_message = "External DNS should be deployed"
  }

  assert {
    condition     = length(helm_release.gitlab_agent) == 0
    error_message = "GitLab Agent should not be installed"
  }
}

run "full_installation" {
  command = apply

  variables {
    install_nginx_ingress = true
    install_cert_manager  = true
    install_external_dns  = true
    install_gitlab_agent  = true
    nginx_helm_values     = "./tests/test-values/nginx-ingress-values.yaml"
    letsencrypt_email     = "letsencrypt@example.com"
    gitlab_agent_token    = "testagenttoken"
    environment           = "test"
    clusterissuer_type    = "both"
  }

  assert {
    condition     = helm_release.nginx_ingress_controller[0].status == "deployed"
    error_message = "NGINX Ingress Controller should be deployed"
  }

  assert {
    condition     = helm_release.cert_manager[0].status == "deployed"
    error_message = "Cert Manager should be deployed"
  }

  assert {
    condition     = helm_release.external_dns[0].status == "deployed"
    error_message = "External DNS should be deployed"
  }

  assert {
    condition     = helm_release.gitlab_agent[0].status == "deployed"
    error_message = "GitLab Agent should be deployed"
  }
}