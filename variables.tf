variable "install_nginx_ingress" {
  description = "(Required) Whether to install NGINX Ingress Controller"
  type        = bool
}

variable "install_cert_manager" {
  description = "(Required) Whether to install Cert Manager"
  type        = bool
}

variable "install_external_dns" {
  description = "(Required) Whether to install External DNS"
  type        = bool
}

variable "install_gitlab_agent" {
  description = "(Required) Whether to install GitLab Kubernetes Agent"
  type        = bool
}

variable "nginx_version" {
  description = "(Optional) The NGINX Ingress Controller version to be deployed"
  type        = string
  default     = "4.11.2"
}

variable "nginx_namespace" {
  description = "(Optional) The namespace for NGINX Ingress Controller"
  type        = string
  default     = "ingress-nginx"
}

variable "cert_manager_namespace" {
  description = "(Optional) The namespace for Cert Manager"
  type        = string
  default     = "cert-manager"
}

variable "nginx_helm_values" {
  description = "(Required) The path to the values file for the NGINX Ingress Chart"
  type        = string
  default     = null
}

variable "cert_manager_version" {
  description = "(Optional) The Cert Manager version to be deployed"
  type        = string
  default     = "v1.16.0"
}

variable "timeout" {
  description = "(Optional) Timeout in seconds for Helm releases"
  type        = number
  default     = 600
}

variable "create_namespace" {
  description = "(Optional) Controls namespace creation for Helm releases"
  type        = bool
  default     = true
}

variable "install_crds" {
  description = "(Optional) Controls installation of custom resource definitions for Cert Manager"
  type        = bool
  default     = true
}

variable "external_dns_version" {
  description = "(Optional) The External DNS version to be deployed"
  type        = string
  default     = "8.3.9"
}

variable "external_dns_namespace" {
  description = "(Optional) The namespace for External DNS"
  type        = string
  default     = "external-dns"
}

variable "external_dns_policy" {
  description = "(Optional) The External DNS policy, sync or upsert-only"
  type        = string
  default     = "sync"
}

variable "cf_token" {
  description = "(Optional) Cloudflare API token"
  type        = string
  sensitive   = true
  default     = null
}

variable "cf_api_key" {
  description = "(Optional) Cloudflare API key"
  type        = string
  sensitive   = true
  default     = null
}

variable "cf_email" {
  description = "(Optional) Cloudflare account email"
  type        = string
  default     = null
}

variable "cf_proxied" {
  description = "(Optional) Whether to proxy Cloudflare DNS records"
  type        = bool
  default     = true
}

variable "environment" {
  description = "(Optional) Environment name for DNS record prefixes"
  type        = string
  default     = null
}

variable "letsencrypt_email" {
  description = "(Optional) Email address for Let's Encrypt notifications"
  type        = string
  default     = null
}

variable "gitlab_agent_version" {
  description = "(Optional) The GitLab Kubernetes Agent version to be deployed"
  type        = string
  default     = "2.7.2"
}

variable "gitlab_agent_namespace" {
  description = "(Optional) The namespace for GitLab Kubernetes Agent"
  type        = string
  default     = "gitlab-agent"
}

variable "gitlab_agent_release_name" {
  description = "(Optional) The name of the GitLab Kubernetes Agent Helm release"
  type        = string
  default     = "gitlab-agent"
}

variable "gitlab_agent_token" {
  description = "(Optional) Token for GitLab Kubernetes Agent"
  type        = string
  sensitive   = true
  default     = null
}

variable "gitlab_kas_address" {
  description = "(Optional) Kas Address for Gitlab Kubernetes Agent"
  type        = string
  default     = "wss://kas.gitlab.com"
}

variable "clusterissuer_type" {
  description = "(Optional) Type of ClusterIssuer to install when cert-manager is enabled. Valid values are 'http', 'dns', or 'both'"
  type        = string
  default     = "both"
  validation {
    condition     = contains(["http", "dns", "both"], var.clusterissuer_type)
    error_message = "Valid values for clusterissuer_type are 'http', 'dns', or 'both'."
  }
}