locals {
  install_nginx_ingress = var.install_nginx_ingress
  install_cert_manager  = var.install_cert_manager
  install_external_dns  = var.install_external_dns
  install_gitlab_agent  = var.install_gitlab_agent

  # Conditional installation of Cluster Issuers
  install_http_issuer = local.install_cert_manager && (var.clusterissuer_type == "http" || var.clusterissuer_type == "both")
  install_dns_issuer  = local.install_cert_manager && (var.clusterissuer_type == "dns" || var.clusterissuer_type == "both")
}

resource "helm_release" "nginx_ingress_controller" {
  count            = local.install_nginx_ingress ? 1 : 0
  name             = "ingress-nginx"
  chart            = "ingress-nginx"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  version          = var.nginx_version
  namespace        = var.nginx_namespace
  timeout          = var.timeout
  create_namespace = var.create_namespace

  values = [
    file(var.nginx_helm_values)
  ]
}

resource "helm_release" "cert_manager" {
  count            = local.install_cert_manager ? 1 : 0
  name             = "cert-manager"
  chart            = "cert-manager"
  repository       = "https://charts.jetstack.io"
  version          = var.cert_manager_version
  namespace        = var.cert_manager_namespace
  create_namespace = var.create_namespace

  set {
    name  = "crds.enabled"
    value = var.install_crds
  }
}

resource "kubectl_manifest" "clusterissuer_letsencrypt_http_prod" {
  count     = local.install_http_issuer ? 1 : 0
  yaml_body = <<-EOF
    apiVersion: cert-manager.io/v1
    kind: ClusterIssuer
    metadata:
      name: letsencrypt-http
    spec:
      acme:
        email: ${var.letsencrypt_email}
        privateKeySecretRef:
          name: letsencrypt-http
        server: https://acme-v02.api.letsencrypt.org/directory
        solvers:
          - http01:
              ingress:
                ingressClassName: nginx
    EOF

  depends_on = [helm_release.cert_manager]
}

resource "kubernetes_secret_v1" "clusterissuer_letsencrypt_dns" {
  count = local.install_dns_issuer ? 1 : 0
  metadata {
    name      = "cf-api-key"
    namespace = var.cert_manager_namespace
  }
  data = {
    api-key = var.cf_api_key
  }
  type = "Opaque"

  depends_on = [helm_release.cert_manager]
}

resource "kubectl_manifest" "clusterissuer_letsencrypt_dns" {
  count     = local.install_dns_issuer ? 1 : 0
  yaml_body = <<-EOF
    apiVersion: cert-manager.io/v1
    kind: ClusterIssuer
    metadata:
      name: letsencrypt-dns
    spec:
      acme:
        email: ${var.letsencrypt_email}
        preferredChain: ""
        privateKeySecretRef:
          name: letsencrypt-dns
        server: https://acme-v02.api.letsencrypt.org/directory
        solvers:
        - dns01:
            cloudflare:
              apiKeySecretRef:
                key: api-key
                name: cf-api-key
              email: ${var.cf_email}
    EOF

  depends_on = [helm_release.cert_manager, kubernetes_secret_v1.clusterissuer_letsencrypt_dns]
}

resource "helm_release" "external_dns" {
  count            = local.install_external_dns ? 1 : 0
  name             = "external-dns"
  chart            = "external-dns"
  repository       = "https://charts.bitnami.com/bitnami"
  version          = var.external_dns_version
  namespace        = var.external_dns_namespace
  create_namespace = var.create_namespace

  set {
    name  = "provider"
    value = "cloudflare"
  }
  set {
    name  = "cloudflare.apiToken"
    value = var.cf_token
  }
  set {
    name  = "cloudflare.apiKey"
    value = var.cf_api_key
  }
  set {
    name  = "cloudflare.email"
    value = var.cf_email
  }
  set {
    name  = "cloudflare.proxied"
    value = var.cf_proxied
  }
  set {
    name  = "txtPrefix"
    value = "${var.environment}-cluster-"
  }
  set {
    name  = "policy"
    value = var.external_dns_policy
  }
}

resource "helm_release" "gitlab_agent" {
  count            = local.install_gitlab_agent ? 1 : 0
  name             = var.gitlab_agent_release_name
  chart            = "gitlab-agent"
  repository       = "https://charts.gitlab.io"
  version          = var.gitlab_agent_version
  namespace        = var.gitlab_agent_namespace
  create_namespace = var.create_namespace

  set {
    name  = "config.token"
    value = var.gitlab_agent_token
  }

  set {
    name  = "config.kasAddress"
    value = var.gitlab_kas_address
  }
}