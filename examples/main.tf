module "kubernetes_addons" {
  source = "git::https://gitlab.com/ccsolutions.io/open-source/terraform/modules/k8s-core-components.git?ref=1.8.1"

  install_nginx_ingress = true
  install_cert_manager  = true
  install_external_dns  = true
  install_gitlab_agent  = false

  nginx_helm_values  = "./path/to/nginx-values.yaml"
  letsencrypt_email  = "your-email@example.com"
  cf_token           = var.cloudflare_token
  cf_api_key         = var.cloudflare_api_key
  cf_email           = "your-cloudflare-email@example.com"
  environment        = "production"
  gitlab_agent_token = var.gitlab_agent_token

  nginx_version           = "4.11.0"
  nginx_namespace         = "ingress-nginx"
  cert_manager_version    = "v1.15.1"
  cert_manager_namespace  = "cert-manager"
  external_dns_version    = "8.2.1"
  external_dns_namespace  = "external-dns"
  cf_proxied              = true
  gitlab_agent_version    = "1.20.0"
  gitlab_agent_namespace  = "gitlab-agent"
  gitlab_agent_release_name = "gitlab-agent"

  timeout          = 600
  create_namespace = true
  install_crds     = true
}