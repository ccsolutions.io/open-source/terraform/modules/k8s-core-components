# K8s Core Components Module

This OpenTofu module manages core Kubernetes components, including NGINX Ingress Controller, Cert Manager, External DNS, and GitLab Kubernetes Agent.

## Features

- Conditional installation of NGINX Ingress Controller, Cert Manager, External DNS, and GitLab Kubernetes Agent
- Configurable Helm chart versions and namespaces
- Support for Cloudflare DNS integration
- Let's Encrypt certificate issuance (HTTP and DNS challenges)
- GitLab Kubernetes Agent integration

## Security Scanning

This module includes security scanning as part of its CI/CD pipeline:

1. **Secret Detection**: Utilizes GitLab's built-in Secret Detection template to identify and prevent accidental commit of secrets.

2. **SAST (Static Application Security Testing) for Infrastructure-as-Code**: Employs GitLab's SAST-IaC template to analyze Terraform/OpenTofu code for potential security issues.

These scans are automatically run as part of the GitLab CI pipeline, helping to ensure the security and integrity of the infrastructure code.

### Viewing Scan Results

- **Secret Detection**: Results can be found in the GitLab CI/CD pipeline under the "secret_detection" job. You can also view detected secrets inside the Pipeline tab "Security".

- **SAST-IaC**: Results are available in the GitLab CI/CD pipeline under the "kics-iac-sast" job. You can also view detected secrets inside the Pipeline tab "Security".

To access these results:
1. Go to your Pipeline in GitLab.
2. Navigate to "Security" tab.
3. View the details of the scan results.

## Pre-commit Hooks

This module uses pre-commit hooks to maintain code quality and consistency. To ensure code quality and prevent issues before they are pushed to the repository, follow these steps:

1. Install the pre-commit tool if you haven't already:
   ```
   pip install pre-commit
   ```

2. Install the git hook scripts:
   ```
   pre-commit install
   ```

3. Before committing any changes, run the following command to execute all pre-commit hooks:
   ```
   pre-commit run -a
   ```

   This command will run all the pre-commit hooks on all files in the repository. It's crucial to run this before pushing any code to ensure consistency and catch potential issues early.

4. If any hooks fail, address the reported issues and repeat step 3 until all checks pass.

5. After all checks pass, you can commit and push your changes.

**Important**: Always run `pre-commit run -a` before pushing your code to the repository. This helps maintain code quality and consistency across the project.

## Usage

To use this module, include it in your OpenTofu configuration and provide the required variables:

```hcl
module "k8s_core_components" {
  source = "path/to/module"

  install_nginx_ingress = true
  install_cert_manager  = true
  install_external_dns  = true
  install_gitlab_agent  = true

  nginx_helm_values   = "./path/to/nginx-values.yaml"
  letsencrypt_email   = "admin@example.com"
  gitlab_agent_token  = "your-gitlab-agent-token"
  environment         = "production"
  cf_token            = "your-cloudflare-token"
  cf_api_key          = "your-cloudflare-api-key"
  cf_email            = "your-cloudflare-email@example.com"
}
```

Refer to the inputs section below for all available configuration options.

---<!-- BEGINNING OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.8.2 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | 2.15.0 |
| <a name="requirement_kubectl"></a> [kubectl](#requirement\_kubectl) | 1.14.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | 2.32.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_helm"></a> [helm](#provider\_helm) | 2.15.0 |
| <a name="provider_kubectl"></a> [kubectl](#provider\_kubectl) | 1.14.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | 2.32.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.cert_manager](https://registry.terraform.io/providers/hashicorp/helm/2.15.0/docs/resources/release) | resource |
| [helm_release.external_dns](https://registry.terraform.io/providers/hashicorp/helm/2.15.0/docs/resources/release) | resource |
| [helm_release.gitlab_agent](https://registry.terraform.io/providers/hashicorp/helm/2.15.0/docs/resources/release) | resource |
| [helm_release.nginx_ingress_controller](https://registry.terraform.io/providers/hashicorp/helm/2.15.0/docs/resources/release) | resource |
| [kubectl_manifest.clusterissuer_letsencrypt_dns](https://registry.terraform.io/providers/gavinbunney/kubectl/1.14.0/docs/resources/manifest) | resource |
| [kubectl_manifest.clusterissuer_letsencrypt_http_prod](https://registry.terraform.io/providers/gavinbunney/kubectl/1.14.0/docs/resources/manifest) | resource |
| [kubernetes_secret_v1.clusterissuer_letsencrypt_dns](https://registry.terraform.io/providers/hashicorp/kubernetes/2.32.0/docs/resources/secret_v1) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cert_manager_namespace"></a> [cert\_manager\_namespace](#input\_cert\_manager\_namespace) | (Optional) The namespace for Cert Manager | `string` | `"cert-manager"` | no |
| <a name="input_cert_manager_version"></a> [cert\_manager\_version](#input\_cert\_manager\_version) | (Optional) The Cert Manager version to be deployed | `string` | `"v1.16.0"` | no |
| <a name="input_cf_api_key"></a> [cf\_api\_key](#input\_cf\_api\_key) | (Optional) Cloudflare API key | `string` | `null` | no |
| <a name="input_cf_email"></a> [cf\_email](#input\_cf\_email) | (Optional) Cloudflare account email | `string` | `null` | no |
| <a name="input_cf_proxied"></a> [cf\_proxied](#input\_cf\_proxied) | (Optional) Whether to proxy Cloudflare DNS records | `bool` | `true` | no |
| <a name="input_cf_token"></a> [cf\_token](#input\_cf\_token) | (Optional) Cloudflare API token | `string` | `null` | no |
| <a name="input_clusterissuer_type"></a> [clusterissuer\_type](#input\_clusterissuer\_type) | (Optional) Type of ClusterIssuer to install when cert-manager is enabled. Valid values are 'http', 'dns', or 'both' | `string` | `"both"` | no |
| <a name="input_create_namespace"></a> [create\_namespace](#input\_create\_namespace) | (Optional) Controls namespace creation for Helm releases | `bool` | `true` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | (Optional) Environment name for DNS record prefixes | `string` | `null` | no |
| <a name="input_external_dns_namespace"></a> [external\_dns\_namespace](#input\_external\_dns\_namespace) | (Optional) The namespace for External DNS | `string` | `"external-dns"` | no |
| <a name="input_external_dns_policy"></a> [external\_dns\_policy](#input\_external\_dns\_policy) | (Optional) The External DNS policy, sync or upsert-only | `string` | `"sync"` | no |
| <a name="input_external_dns_version"></a> [external\_dns\_version](#input\_external\_dns\_version) | (Optional) The External DNS version to be deployed | `string` | `"8.3.9"` | no |
| <a name="input_gitlab_agent_namespace"></a> [gitlab\_agent\_namespace](#input\_gitlab\_agent\_namespace) | (Optional) The namespace for GitLab Kubernetes Agent | `string` | `"gitlab-agent"` | no |
| <a name="input_gitlab_agent_release_name"></a> [gitlab\_agent\_release\_name](#input\_gitlab\_agent\_release\_name) | (Optional) The name of the GitLab Kubernetes Agent Helm release | `string` | `"gitlab-agent"` | no |
| <a name="input_gitlab_agent_token"></a> [gitlab\_agent\_token](#input\_gitlab\_agent\_token) | (Optional) Token for GitLab Kubernetes Agent | `string` | `null` | no |
| <a name="input_gitlab_agent_version"></a> [gitlab\_agent\_version](#input\_gitlab\_agent\_version) | (Optional) The GitLab Kubernetes Agent version to be deployed | `string` | `"2.7.2"` | no |
| <a name="input_gitlab_kas_address"></a> [gitlab\_kas\_address](#input\_gitlab\_kas\_address) | (Optional) Kas Address for Gitlab Kubernetes Agent | `string` | `"wss://kas.gitlab.com"` | no |
| <a name="input_install_cert_manager"></a> [install\_cert\_manager](#input\_install\_cert\_manager) | (Required) Whether to install Cert Manager | `bool` | n/a | yes |
| <a name="input_install_crds"></a> [install\_crds](#input\_install\_crds) | (Optional) Controls installation of custom resource definitions for Cert Manager | `bool` | `true` | no |
| <a name="input_install_external_dns"></a> [install\_external\_dns](#input\_install\_external\_dns) | (Required) Whether to install External DNS | `bool` | n/a | yes |
| <a name="input_install_gitlab_agent"></a> [install\_gitlab\_agent](#input\_install\_gitlab\_agent) | (Required) Whether to install GitLab Kubernetes Agent | `bool` | n/a | yes |
| <a name="input_install_nginx_ingress"></a> [install\_nginx\_ingress](#input\_install\_nginx\_ingress) | (Required) Whether to install NGINX Ingress Controller | `bool` | n/a | yes |
| <a name="input_letsencrypt_email"></a> [letsencrypt\_email](#input\_letsencrypt\_email) | (Optional) Email address for Let's Encrypt notifications | `string` | `null` | no |
| <a name="input_nginx_helm_values"></a> [nginx\_helm\_values](#input\_nginx\_helm\_values) | (Required) The path to the values file for the NGINX Ingress Chart | `string` | `null` | no |
| <a name="input_nginx_namespace"></a> [nginx\_namespace](#input\_nginx\_namespace) | (Optional) The namespace for NGINX Ingress Controller | `string` | `"ingress-nginx"` | no |
| <a name="input_nginx_version"></a> [nginx\_version](#input\_nginx\_version) | (Optional) The NGINX Ingress Controller version to be deployed | `string` | `"4.11.2"` | no |
| <a name="input_timeout"></a> [timeout](#input\_timeout) | (Optional) Timeout in seconds for Helm releases | `number` | `600` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
